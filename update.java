package com.example.uapv1600843.tp3;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class update extends AsyncTask<City, Integer, City[]> {

    Runnable run;

    public update(Runnable run){
        this.run = run;
    }

    @Override
    protected City[] doInBackground(City... C) {
        int count = C.length;
        City[] Cresult = new City[count];
        for (int i = 0; i < count; i++) {

            URL url = null;
            try {
                url = WebServiceUrl.build(C[i].getName(),C[i].getCountry());
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                try {
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                    JSONResponseHandler js = new JSONResponseHandler(C[i]);
                    Cresult [i] = js.readJsonStream(in);

                }
                finally {
                    urlConnection.disconnect();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            // Escape early if cancel() is called
            if (isCancelled()) break;
        }
        Log.d("app",Cresult.toString());
        return Cresult;
    }



    @Override
    protected void onPostExecute(City[] Cresult) {
        super.onPostExecute(Cresult);
        this.run.run();
    }


}
