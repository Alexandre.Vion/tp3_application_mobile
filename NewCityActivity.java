package com.example.uapv1600843.tp3;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

//import fr.uavignon.shuet.tp3.data.City;

public class NewCityActivity extends AppCompatActivity {

    private EditText textName, textCountry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_city);

        textName = (EditText) findViewById(R.id.editNewName);
        textCountry = (EditText) findViewById(R.id.editNewCountry);

        final Button but = (Button) findViewById(R.id.button);

        but.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                City city = new City (textName.getText().toString(),textCountry.getText().toString());
                Bundle bundle = new Bundle();
                //bundle.putString("name",textName.getText().toString());
                //bundle.putString("country",textCountry.getText().toString());
                bundle.putParcelable("city",city);
                intent.putExtra("newCity",bundle);
                setResult(MainActivity.RESULT_OK,intent);
                finish();
            }
        });
    }


}
