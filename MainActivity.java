package com.example.uapv1600843.tp3;

import android.content.Intent;
import android.database.Cursor;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import java.util.concurrent.ExecutionException;

import static com.example.uapv1600843.tp3.R.id.add;
import static com.example.uapv1600843.tp3.R.id.listcountry;
import static com.example.uapv1600843.tp3.R.id.swiperefresh;
import static com.example.uapv1600843.tp3.WeatherDbHelper.COLUMN_CITY_NAME;

public class MainActivity extends AppCompatActivity {

    SimpleCursorAdapter cursorAdapter;
    WeatherDbHelper dbHelper;
    SwipeRefreshLayout refresh;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        refresh = (SwipeRefreshLayout) findViewById(swiperefresh);

        final ListView listview = (ListView) findViewById(listcountry);
        final FloatingActionButton button = (FloatingActionButton) findViewById(add);
         dbHelper = new WeatherDbHelper(this);
        //dbHelper.populate();
        final Cursor cursor = dbHelper.fetchAllCities();
         cursorAdapter= new SimpleCursorAdapter(this,
                android.R.layout . simple_list_item_2,
                dbHelper.fetchAllCities()    ,
                new String []{WeatherDbHelper.COLUMN_CITY_NAME,WeatherDbHelper.COLUMN_COUNTRY},
         new int[]{android.R.id.text1  ,android.R.id.text2});
        listview.setAdapter(cursorAdapter);
        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                final Cursor cursor = dbHelper.fetchAllCities();
                City [] C= new City[1];
                while (!cursor.isAfterLast())
                {
                    try {
                        C = new update(new Runnable(){
                    @Override
                    public void run(){

                    }
                }).execute(dbHelper.cursorToCity(cursor)).get();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }
                    dbHelper.updateCity(C[0]);
                    cursor.moveToNext();
                }
                recreate();

            }
        });


        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                cursor.moveToFirst();
                Intent intent = new Intent (MainActivity.this,CityActivity.class);
                Bundle bundle = new Bundle();
                cursor.move(position);
                City city = dbHelper.cursorToCity(cursor);
                intent.putExtra("city",city);
                startActivityForResult(intent,2);
            }
        });


        listview.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)
            {
                MenuInflater inflater = getMenuInflater();
                inflater.inflate(R.menu.city, menu);
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, NewCityActivity.class);
                startActivityForResult(intent,1);
            }

        });
    }

    @Override
    protected void onActivityResult(int requestCode,int resultCode, Intent data)
    {
        if (requestCode == 1)
        {
            if ( resultCode == MainActivity.RESULT_OK)
            {
                Bundle bundle = data.getBundleExtra("newCity");
                City city = bundle.getParcelable("city");
                dbHelper.addCity(city);
                final ListView listView = (ListView) findViewById(listcountry);
                cursorAdapter.notifyDataSetChanged();
                listView.setAdapter(cursorAdapter);
                recreate();
            }
        }
        if (requestCode == 2)
        {

            if ( resultCode == MainActivity.RESULT_OK)
            {
                Bundle bundle = data.getBundleExtra("updateCity");
                City city = bundle.getParcelable("city");
                dbHelper.updateCity(city);
                final ListView listView = (ListView) findViewById(listcountry);
                cursorAdapter.notifyDataSetChanged();
                listView.setAdapter(cursorAdapter);
                recreate();
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item)
    {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        final Cursor cursor = dbHelper.fetchAllCities();
        cursor.move(info.position);
        dbHelper.deleteCity(cursor);
        recreate();
        return true;
    }




}

